#pragma once

#include "key.h"

#include <memory>
#include <string>

enum CryptoDirection {
    ENCRYPT, DECRYPT
};

class CryptoFile {
private:
    std::string inputFilePath;
    std::string outputFilePath;
    std::shared_ptr<Key> key;
    CryptoDirection direction;
    char translateChar(char ch) const;
public:
    CryptoFile(const std::string &inputFilePath,
               const std::string &outputFilePath,
               std::shared_ptr<Key> key,
               CryptoDirection direction);
    bool translate() const;
};
