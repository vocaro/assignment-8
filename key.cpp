#include "key.h"

const int ROTATION_AMOUNT = 10;

// Encrypts a single byte, returning the encrypted value.
char Key::encrypt(char ch) const
{
    return ch + ROTATION_AMOUNT;
}

// Decrypts a single byte, returning the decrypted value.
char Key::decrypt(char ch) const
{
    return ch - ROTATION_AMOUNT;
}
