#include "cryptofile.h"

#include <fstream>

using namespace std;

// Initializes the CryptoFile object. Specify either ENCRYPT or DECRYPT
// as the translation direction.
CryptoFile::CryptoFile(const string &inputFilePath,
                       const string &outputFilePath,
                       shared_ptr<Key> key,
                       CryptoDirection direction)
{
    this->inputFilePath = inputFilePath;
    this->outputFilePath = outputFilePath;
    this->key = key;
    this->direction = direction;
}

// Translates the input file to the output file using the `direction`
// specified on creation. Returns true if translation was successul;
// false otherwise.
bool CryptoFile::translate() const
{
    fstream inFile(inputFilePath, ios::binary | ios::in);
    fstream outFile(outputFilePath, ios::binary | ios::out | ios::trunc);

    if (!inFile || !outFile) {
        return false;
    }

    char ch;
    inFile.get(ch);

    // Read through the entire input file, stopping only if an error occurs.
    while (!inFile.eof()) {

        if (inFile.bad()) {
            return false;
        }

        outFile.put(translateChar(ch));

        if (outFile.bad()) {
            return false;
        }
        
        inFile.get(ch);
    }

    return true;
}

// Converts a byte of data according to the direction specified.
char CryptoFile::translateChar(char ch) const 
{
    switch (direction) {
        case ENCRYPT:
        return key->encrypt(ch);

        case DECRYPT:
        return key->decrypt(ch);
    }  
}
