#include "cryptofile.h"
#include "key.h"

#include <iostream>

using namespace std;

int main(int argc, char **argv)
{
    if (argc != 3) {
        cout << "Usage: decrypt INPUTFILE OUTPUTFILE" << endl;
        return 1;
    }

    string inputFilePath = argv[1];
    string outputFilePath = argv[2];
    
    CryptoFile cryptoFile(inputFilePath, outputFilePath, make_shared<Key>(), DECRYPT);
    if (!cryptoFile.translate()) {
        cerr << "Error encountered while decrypting" << endl;
        return 1;
    }
    
    return 0;
}
