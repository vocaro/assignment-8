#pragma once

class Key {
public:
    char encrypt(char ch) const;
    char decrypt(char ch) const;  
};
